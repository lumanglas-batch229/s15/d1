// JS Comments ctrl + /
// They are important, acts as a  guide for the programmer.
// Comments are diregarded no matter how long it may be.
	// Multi-line comments ctrl + shift + /
/* This
is 
a 
multi
line
comment*/

// STATEMENTS 
// Programming Instruction that we tell the computer to perform.
// Statements usually ends with Semicolon (;)

// Syntax
// It is the set of rules that describes how statements must be constructed.


// alert ("Hello Again!");

// This is  a statement with a correct syntax
console.log("Hello World!");

// JS  is a loose type programming language
console. log (  "  Hello World!  "  );

// And also with this
console.
log
(
"Hello Again"
)

// [SECTION] Variables
// It is used as container or storage.

// Declaring Variables - tells our device that a variable name is created and is ready to store data.

// Syntax - let/const variableName;

// "Let is a keyword that is usually used to declare a variable."

let myVariable;
let	hello;

console.log(myVariable);
console.log(hello);

// = sign stands for initialization, that means giving a value to a variable


// Guidlines for declaring a Variable
/*
1. Use Let keyword followed by a variable name and then followed again by the assignment =.
2. Variable Names should start with a lowercase letter.
3. For constant variable or const we are using const keyword. 
4. Variable names should be comprehensive or descriptive.
5. Dont use spaces on the declared Variables.
6. All Variable Name should be Unique.
*/


// Difference Casing Styles
// Cammel case - thisIsCammelCasing
// Snake case - this_is_snake_casing
// Kebab case - this-is-kebab-casing

// this is a good variable name
let firstName = "Michael";

//this is bad variable name
let	pokemon = 25000;

// Declaring and initializing variables
// Syntax -> let/const variableName = value;

let productName ="desktop computer";
console.log(productName);

let productPrice = 18999;
console.log (productPrice)

// re-assigning value to variable.

productPrice = 25000;
console.log(productPrice);
console.log(productPrice);
console.log(productPrice);


let friend ="kate";
friend = "jane";
console.log(friend);

let supplier;

supplier = "John Smith Tradings"
console.log(supplier);

supplier = "Zuitt Store";
console.log(supplier);

//let vs const
// let variables values can be changed, we can re-assign new values.
// const variables values cannot be changed.

const	hoursInAday = 24;
console.log(hoursInAday);

const pie = 3.14;
console.log(pie);

// Local.global scope Variables
let outerVariable = "Hi"; //This is a Global Variable

{
	 let innerVariable = "Hello Again"; // This is a local Variable
	 console.log(innerVariable);
	 console.log(outerVariable);

}

console.log(outerVariable);
//console.log(innerVariable);This will throw error, innerVariable is not accessible since it is enclosed with curly braces

//Multiple Variable Decleration
// Multiple Variable may be Declared in one line.
// Convinient and it is easier to read the code.

/*let productCode = "DC017";
let productBrand = "Dell";*/

let productCode = "DC017" , productBrand = "Dell";
console.log	(productCode, productBrand);

// Let is a reserved keyword, will be having an error
/*const let = "Hello";
console.log(let);*/

// [SECTION] Data Types

// Strings - are series of characters that creates a word.

let country ="Philippines";
let province = "Metro Manila";

// Concatenating String, Using + Symbol
// Combining String Values

//Metro Manila,
let fullAddress = province + ", " + country;
console.log(fullAddress);

let greeting = "I live in the " + country;
console.log(greeting);

// esacpe character (/)
// "\n" refers to a creates a new line between text

let mailAddress = "Metro Manila \n\nPhilippines";
console.log(mailAddress);

let message = "John's Employees went home early";
console.log(message);
message = 'John\'s employess went home early';
console.log(message);

//Numbers
// Integers or Whole Numbers

let headCount = 26;
console.log(headCount);

// decimal or fractions
let grade = 98.7;
console.log(grade);

//Exponential Notation
let planetDistance = 2e10;
console.log(planetDistance);

// Combining text and Strings
console.log("John's grade last quarter is " + grade);

//Bollean 
// We have 2 Boolean values, true and false

let isMarried = false;
let isGoodConduct = true;

console.log("isMarried:" + isMarried);
console.log("isGoodConduct:" + isGoodConduct);

// Arrays
// Are Special kinds of data type
// Used to store multiple values with the same data types
// Syntax -> let/const arrayName = [elementA, elementB,....];

let grades = [98.7, 92.1, 90.2];
console.log(grades);

let details = ["John", "Smith", 32, true];
console.log(details);

// Objects
// Holds properties that describes the variable
// Syntax ->  let/const objectName = {propertyA: value, propertyB: value};

let person = 
{
	fullName: "Juan Dela Cruz",
	age: 35,
	contact: ["0123456789", "987654321"],
	address: {
		houseNumber: "345",
		city: "Manila",
			}
};

console.log(person);

let myGrade = {
	firstGrading:98.7,
	secondGrading: 92.1,
	thirdGrading: 90.2,
	fourthGrading: 94.6
}
console.log(myGrade);

//checking of Data Type
console.log(typeof grades);
console.log(typeof person);
console.log(typeof isMarried);

// in programming we always start counting from 0
const anime = ["one piece", "One punch man", "Attack on Titan"];
// anime = ["Kimetsu no Yaiba"];
anime[0] = "Kimetsu no Yaiba";

console.log(anime);


// Null vs Undefined
// Null when variable has 0 or empty value
//Undefined when a variable has no value upon declaration
let spouse = 0;
let fullName; //Undefined no data yet or value